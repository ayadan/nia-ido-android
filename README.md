# Nia Ido

Ido language utility for Android. Includes offline dictionary and
links to other resources.

## Strings file

If there are language errors, please make a fork and update the strings file:

```https://github.com/Aya-Dan/Nia-Ido/blob/master/Nia-Ido/app/src/main/res/values/strings.xml```

and then make a pull request. Thank you!

## Download app

* [Google Play](https://play.google.com/store/apps/details?id=niaido.ayadan.com.niaido)
* [Itch.io](https://moosader.itch.io/nia-ido)
* [GitHub Release](https://github.com/Aya-Dan/Nia-Ido/releases/tag/1.0)

## Screenshots

![Main menu](https://github.com/Aya-Dan/Nia-Ido/blob/master/screenshots/Screenshot%20at%202018-03-31%2023-16-30.png?raw=true)

![Dictionary](https://github.com/Aya-Dan/Nia-Ido/blob/master/screenshots/Screenshot%20at%202018-03-31%2023-18-00.png?raw=true)

![Links](https://github.com/Aya-Dan/Nia-Ido/blob/master/screenshots/Screenshot%20at%202018-03-31%2023-18-13.png?raw=true)
