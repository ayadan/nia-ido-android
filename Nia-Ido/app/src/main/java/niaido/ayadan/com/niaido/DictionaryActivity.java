package niaido.ayadan.com.niaido;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DictionaryActivity extends AppCompatActivity {

    List<String> dictionary = new ArrayList<String>();
    Map<String, Integer> typeToFile = new HashMap<String,Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        typeToFile.put("Ido > Deutsch",     R.raw.ido_to_de);
        typeToFile.put("Deutsch > Ido",     R.raw.de_to_ido);
        typeToFile.put("Ido > English",     R.raw.ido_to_en);
        typeToFile.put("English > Ido",     R.raw.en_to_ido);
        typeToFile.put("Ido > Esperanto",   R.raw.ido_to_eo);
        typeToFile.put("Esperanto > Ido",   R.raw.eo_to_ido);
        typeToFile.put("Ido > Français",    R.raw.ido_to_fr);
        typeToFile.put("Ido > Interlingua", R.raw.ido_to_interlingua);
        typeToFile.put("Ido > Italiano",    R.raw.ido_to_it);
        typeToFile.put("Ido > Nederlands",  R.raw.ido_to_nld);
        typeToFile.put("Ido > 日本語",      R.raw.ido_to_jp);
        typeToFile.put("日本語 > Ido",      R.raw.jp_to_ido);
        typeToFile.put("Ido > Portugues",   R.raw.ido_to_pt);
        typeToFile.put("Ido > русский",     R.raw.ido_to_ru);
        typeToFile.put("Ido > Suomi",       R.raw.ido_to_fi);

        setupDropdowns();
        loadDictionary();
    }

    protected void setupDropdowns() {
        List<String> list = new ArrayList<String>();
        list.add("Ido > Deutsch");
        list.add("Deutsch > Ido");
        list.add("Ido > English");
        list.add("English > Ido");
        list.add("Ido > Esperanto");
        list.add("Esperanto > Ido");
        list.add("Ido > Français");
        list.add("Français > Ido");
        list.add("Ido > 日本語");
        list.add("日本語 > Ido");
        list.add("Ido > Interlingua");
        list.add("Interlingua > Ido");
        list.add("Ido > Italiano");
        list.add("Ido > Nederlands");
        list.add("Ido > Portugues");
        list.add("Ido > русский");
        list.add("Ido > Suomi");

        final Spinner sp1 = (Spinner) findViewById(R.id.spnDictionary);

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adp1);
        sp1.setSelection(3);

        List<String> methods = new ArrayList<String>();
        methods.add("Unesma vorto");
        methods.add("Tota paragrafo");
        //methods.add("Regex");

        final Spinner sp3 = (Spinner) findViewById(R.id.spnMethod);

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, methods);
        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3.setAdapter(adp2);
    }

    public void loadDictionary()
    {
        final Spinner spFrom = (Spinner) findViewById(R.id.spnDictionary);

        String dictType = spFrom.getSelectedItem().toString();
        int fileResource = typeToFile.get( dictType );

        Resources res = getResources();

        InputStream inStream = res.openRawResource(fileResource);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

        dictionary.clear();

        String results = "";

        try {
            String line = reader.readLine();
            line = "bob";
            while (line != null) {
                line = reader.readLine();

                if (line != null ) {
                    dictionary.add(line);
                }
            }
            results = "Kompleta";
        } catch (IOException e) {
            e.printStackTrace();
            results = "Dessuceso";
        }

        TextView text = (TextView) findViewById(R.id.txtResults);
        text.setText(results);
    }

    public void searchButton( View v ) {
        final TextView txtSearch = (TextView) findViewById(R.id.txtSearch);
        String search = txtSearch.getText().toString();

        TextView text = (TextView) findViewById(R.id.txtResults);
        text.setText("Vartez...");

        search( v, search );
    }

    public void search( View v, String searchTerm ) {
        String results = "";

        Spinner spMethod = (Spinner) findViewById(R.id.spnMethod);
        TextView text = (TextView) findViewById(R.id.txtResults);

        for ( int i = 0; i < dictionary.size(); i++ )
        {
            String line = dictionary.get(i).toUpperCase();

            if ( line == "" || line == null )
            {
                continue;
            }

            String search = searchTerm.toUpperCase();
            int firstSpace = line.indexOf(' ');

            Boolean match = false;

            try {
                if (firstSpace < 0) {
                    match = false;
                }
                else {
                    match = (spMethod.getSelectedItemPosition() == 0 && line.substring(0, firstSpace - 1).equals(search));
                }
            }
            catch( Exception ex )
            {
                results += ex.toString() + System.getProperty("line.separator");
                results += "Length: " + line.length() + System.getProperty("line.separator");
                results += "Text: \"" + line + "\"" + System.getProperty("line.separator");
                text.setText(results);
                return;
            }

            match = match ? true : (spMethod.getSelectedItemPosition() == 1 && line.contains(search) );

            try
            {
                match = match ? true : (spMethod.getSelectedItemPosition() == 2 && dictionary.get(i).matches(searchTerm));
            }
            catch( Exception ex )
            {
                results += ex.toString() + System.getProperty("line.separator");
                text.setText(results);
                return;
            }

            if (match)
            {
                results += dictionary.get(i) + System.getProperty("line.separator") + System.getProperty("line.separator");
            }
        }

        text.setText(results);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }
}
