package niaido.ayadan.com.niaido;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    public void gotoDictionary(View view)
    {
        Intent intent = new Intent(this, DictionaryActivity.class);
        startActivity(intent);
    }

    public void gotoAbout(View view)
    {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void gotoGroups(View view)
    {
        Intent intent = new Intent(this, GroupsActivity.class);
        startActivity(intent);
    }

    public void gotoLessons(View view)
    {
        Intent intent = new Intent(this, LessonsActivity.class);
        startActivity(intent);
    }

    public void gotoEntertainent(View view)
    {
        Intent intent = new Intent(this, EntertainmentActivity.class);
        startActivity(intent);
    }

    public void gotoGitHub(View view)
    {
        Uri uriUrl = Uri.parse("https://github.com/Aya-Dan/Nia-Ido");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
