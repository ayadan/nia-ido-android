package niaido.ayadan.com.niaido;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LessonsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void gotoEasyIdo(View view)
    {
        gotoUrl("https://en.wikibooks.org/wiki/Easy_Ido");
    }

    public void gotoIdoForAll(View view)
    {
        gotoUrl("http://ayadan.moosader.com/archive/ido_for_all.pdf");
    }

    public void gotoBasicGrammar(View view)
    {
        gotoUrl("http://idolinguo.org.uk/bgrammar.htm");
    }

    public void gotoCompleteManual(View view)
    {
        gotoUrl("http://interlanguages.net/manual.html");
    }

    public void gotoQuickIdo(View view)
    {
        gotoUrl("http://interlanguages.net/Grammar.html");
    }

    public void gotoSimplido(View view)
    {
        gotoUrl("http://ayadan.moosader.com/archive/Simplido-v005.pdf");
    }

    public void gotoKanario(View view)
    {
        gotoUrl("http://kanaria1973.ido.li/publikaji/curso2.pdf");
    }

    public void gotoUrl(String url)
    {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
