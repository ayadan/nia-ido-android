package niaido.ayadan.com.niaido;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EntertainmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainment);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void gotoYouTubeAyaDan(View view)
    {
        gotoUrl("https://www.youtube.com/ayadanconlangs");
    }

    public void gotoYouTubeNeil(View view)
    {
        gotoUrl("https://www.youtube.com/user/iolair1973/videos");
    }

    public void gotoYouTubeVidei(View view)
    {
        gotoUrl("https://www.youtube.com/user/VideiEnIdo/videos");
    }

    public void gotoYouTubeIdala(View view)
    {
        gotoUrl("https://www.youtube.com/user/Idiomodiomni/videos");
    }

    public void gotoYouTubeBrian(View view)
    {
        gotoUrl("https://www.youtube.com/channel/UCGyhQE-5blWOAg1AaJ8_VcQ/videos");
    }

    public void gotoPepper(View view)
    {
        gotoUrl("https://www.peppercarrot.com/io/article234/potion-of-flight");
    }

    public void gotoUrl(String url)
    {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
